/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.suphakorn.robotproject2;

/**
 *
 * @author exhau
 */
public class Robot {

    private int x;
    private int y;
    private char symbol;
    private TableMap map;

    public Robot(int x, int y, char symbol, TableMap map) {
        this.x = x;
        this.y = y;
        this.symbol = symbol;
        this.map = map;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return symbol;
    }

    public boolean walk(char direction) {
        switch (direction) {
            case 'w':
            case 'N':
                if (walkN()) return false;
                break;
            case 's':
            case 'S':
                if (walkS()) return false;
                break;
            case 'E':
            case 'd':
                if (walkE()) return false;
                break;
            case 'a':
            case 'W':
                if (walkW()) return false;
                break;
        }
        if(map.isBomb(x,y)){
            printFomdBomb();
        }
        return true;
    }

    private void printFomdBomb() {
        System.out.println("Founded Bomb!!!("+x+", "+ y + ")");
    }

    private boolean walkW() {
        if (map.inMap(x - 1, y)) {
            x = x - 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkE() {
        if (map.inMap(x + 1, y)) {
            x = x + 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkS() {
        if (map.inMap(x, y + 1)) {
            y = y + 1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkN() {
        if (map.inMap(x, y - 1)) {
            y = y - 1;
        } else {
            return true;
        }
        return false;
    }

    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }
}
